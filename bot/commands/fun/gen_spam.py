import math
import random
import re
from typing import List, Tuple

from bot.base import Command
from bot.config import Embed


def contains_mention(string: str) -> Tuple[bool, List[int]]:
    """Checks if a string (discord message) contains a ping

    [Args]:
        string (str): discord message

    [Returns]:
        (bool, List[int]):
            (bool): if one or more ping is found in the string
            (List[int]): list of user IDs that were ping
    """

    pinged_people: List[int] = [found for found in re.findall("<@[0-9]+>", string)]
    return bool(pinged_people), pinged_people


class cmd(Command):
    """
    INFO: This is a spamming function. Very cool j4f.
    Original authors: imindMan + demir.
    """

    name = "gen_spam"
    usage = "gen_spam [spam_string]"
    description = "Generates spam messages for fun"

    def literal_spam(self, string: str):
        # Generate spam till the end of the universe (message length limit)
        output = ""
        spam_len_limit = 2000
        final_chop = spam_len_limit
        chop_case = [
            " ",
            "<",
            ">",
            "\n",
        ]  # Order of the elements determine final chop, first checks first

        for x in range(0, spam_len_limit):
            output += string

        # early chop to work with less stuff in next step
        output = output[:spam_len_limit]

        # version 1:
        # return output

        # Chop at the last character that matched with ordered chop_case
        for x in range(spam_len_limit - 1, 0, -1):
            if output[x] in chop_case:
                final_chop = x
                break

        output = output[:final_chop]
        return output

    async def execute(self, arguments, message) -> None:
        if contains_mention(message.content):
            return

        output = self.literal_spam(arguments[0])
        await message.channel.send(output)
